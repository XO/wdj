/**
 * @author Lance Snider - lance@lancesnider.com
*/

//editable vars
var goalAmount1 = 100;//how much are you trying to get
//var currentAmount2 = 1267;//how much do you currently have (if you want to define in js, not html)
var animationTime1 = 3000;//in milliseconds
var numberPrefix1 = "";//what comes before the number (set to "" if no prefix)
var numberSuffix1 = "";//what goes after the number
var tickMarkSegementCount1 = 4;//each segement adds 40px to the height
var widthOfNumbers1 = 50;//the width in px of the numbers on the left

//standard resolution images
var glassTopImg1 = "wd2/images/glassTop.png";
var glassBodyImg1 = "wd2/images/glassBody.png";
var redVerticalImg1 = "wd2/images/redVertical.png";
var tooltipFGImg1 = "wd2/images/tickShine.png";
var glassBottomImg1 = "wd2/images/glassBottom.png";
var tootipPointImg1 = "wd2/images/tooltipPoint.png";
var tooltipMiddleImg1 = "wd2/images/tooltipMiddle.png";
var tooltipButtImg1 = "wd2/images/tooltipButt.png";

//high res images
var glassTopImg12x1 = "wd2/images/glassTop2x.png";
var glassBodyImg12x1 = "wd2/images/glassBody2x.png";
var redVerticalImg12x1 = "wd2/images/redVertical2x.png";
var tooltipFGImg12x1 = "wd2/images/tickShine2x.png";
var glassBottomImg12x1 = "wd2/images/glassBottom2x.png";
var tootipPointImg12x1 = "wd2/images/tooltipPoint2x.png";
var tooltipMiddleImg12x1 = "wd2/images/tooltipMiddle2x.png";
var tooltipButtImg12x1 = "wd2/images/tooltipButt2x.png";

/////////////////////////////////////////
// ------ don't edit below here ------ //
/////////////////////////////////////////

var arrayOfImages1;
var imgsLoaded1 = 0;
var tickHeight1 = 40;
var mercuryHeight1Empty1 = 0;
var numberStartY1 = 6;
var thermTopHeight1 = 13;
var thermBottomHeight1 = 51;
var tooltipOffset1 = 15; 
var heightOfBody1;
var mercuryId1;
var tooltipId1;
var resolution2x1 = false;

//start once the page is loaded
$( document ).ready(function() {
	determineImageSet1();
});

//this checks if it's the high or normal resolution images
function determineImageSet1(){
	
	resolution2x1 = window.devicePixelRatio == 2;//check if resolution2x1
	
	if(resolution2x1){	
		//switch the regular for 2x res graphics
		glassTopImg1 = glassTopImg12x1;
		glassBodyImg1 = glassBodyImg12x1;
		redVerticalImg1 = redVerticalImg12x1;
		glassBottomImg1 = glassBottomImg12x1;
		tootipPointImg1 = tootipPointImg12x1;
		tooltipButtImg1 = tooltipButtImg12x1;	
	}
	
	createGraphics1();
}

//visually create the thermometer
function createGraphics1(){
	
	//add the html
	$("#goal-thermometer1").html(
		"<div id='therm-numbers1'>" + 
		"</div>" + 
		"<div id='therm-graphics1'>" + 
			"<img id='therm-top1' src='"+glassTopImg1+"'></img>" + 
			"<img id='therm-body-bg1' src='"+glassBodyImg1+"' ></img>" + 
			"<img id='therm-body-mercury1' src='"+redVerticalImg1+"'></img>" + 
			"<div id='therm-body-fore1'></div>" + 
			"<img id='therm-bottom1' src='"+glassBottomImg1+"'></img>" + 
			"<div id='therm-tooltip1'>" + 
				"<img class='tip-left1' src='"+tootipPointImg1+"'></img>" + 
				"<div class='tip-middle1'><p>$0</p></div>" + 
				"<img class='tip-right1' src='"+tooltipButtImg1+"'></img>" + 
			"</div>" + 
		"</div>"
	);
	
	//preload1 and add the background images
	$('<img/>').attr('src', tooltipFGImg1).load(function(){
		$(this).remove();
		$("#therm-body-fore1").css("background-image", "url('"+tooltipFGImg1+"')");
		checkIfAllImagesLoaded1();
	});
	
	$('<img/>').attr('src', tooltipMiddleImg1).load(function(){
		$(this).remove();
		$("#therm-tooltip1 .tip-middle1").css("background-image", "url('" + tooltipMiddleImg1 + "')");
		checkIfAllImagesLoaded1();
	});
	
	//adjust the css
	heightOfBody1 = tickMarkSegementCount1 * tickHeight1;
	$("#therm-graphics1").css("left", widthOfNumbers1)
	$("#therm-body-bg1").css("height", heightOfBody1);
	$("#goal-thermometer1").css("height",  heightOfBody1 + thermTopHeight1 + thermBottomHeight1);
	$("#therm-body-fore1").css("height", heightOfBody1);
	$("#therm-bottom1").css("top", heightOfBody1 + thermTopHeight1);
	mercuryId1 = $("#therm-body-mercury1");
	mercuryId1.css("top", heightOfBody1 + thermTopHeight1);
	tooltipId1 = $("#therm-tooltip1");
	tooltipId1.css("top", heightOfBody1 + thermTopHeight1 - tooltipOffset1);
	
	//add the numbers to the left
	var numbersDiv1 = $("#therm-numbers1");
	var countPerTick1 = goalAmount1/tickMarkSegementCount1;
	var commaSepCountPerTick1 = commaSeparateNumber1(countPerTick1);
	
	//add the number
	for ( var i = 0; i < tickMarkSegementCount1; i++ ) {
		
		var yPos1 = tickHeight1 * i + numberStartY1;
		var style1 = $("<style1>.pos" + i + " { top: " + yPos1 + "px; width:"+widthOfNumbers1+"px }</style1>");
		$("html > head").append(style1);
		var dollarText1 = commaSeparateNumber1(goalAmount1 - countPerTick1 * i);
		$( numbersDiv1 ).append( "<div class='therm-number1 pos" + i + "'>" +dollarText1+ "</div>" );
		
	}
	
	//check that the images are loaded before anything
	arrayOfImages1 = new Array( "#therm-top1", "#therm-body-bg1", "#therm-body-mercury1", "#therm-bottom1", ".tip-left1", ".tip-right1");
	preload1(arrayOfImages1);
	
};

//check if each image is preloaded
function preload1(arrayOfImages1) {
	
	for(i=0;i<arrayOfImages1.length;i++){
		$(arrayOfImages1[i]).load(function() {   checkIfAllImagesLoaded1();  });
	}
    
}

//check that all the images are preloaded
function checkIfAllImagesLoaded1(){
	imgsLoaded1++;
	if(imgsLoaded1 == arrayOfImages1.length+2){
		$("#goal-thermometer1").fadeTo(1000, 1, function(){
			animateThermometer1();
		});
	}
}


//animate the thermometer
function animateThermometer1(){
	
	var percentageComplete1 = currentAmount2/goalAmount1;
	var mercuryHeight1 = Math.round(heightOfBody1 * percentageComplete1); 
	var newMercuryTop1 = heightOfBody1 + thermTopHeight1 - mercuryHeight1;
	
	mercuryId1.animate({height:mercuryHeight1 +1, top:newMercuryTop1 }, animationTime1);
	tooltipId1.animate({top:newMercuryTop1 - tooltipOffset1}, {duration:animationTime1});
	
	var tooltipTxt1 = $("#therm-tooltip1 .tip-middle1 p");
	
	//change the tooltip number as it moves
	$({tipAmount: 0}).animate({tipAmount: currentAmount2}, {
		duration:animationTime1,
		step:function(){
			tooltipTxt1.html(commaSeparateNumber1(this.tipAmount));
		}
	});
	
	
}

//format the numbers with $ and commas
function commaSeparateNumber1(val){
	val = Math.round(val);
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return numberPrefix1 + val + numberSuffix1;
}
