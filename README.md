# H5仪表盘，温度计，风向表

#### 介绍
H5编写的动态获取温度值，仪表盘数据、风向标等数据，编写了ajax请求，并编写了解析，直接拿来使用即可

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/1012/164938_6dfa8d46_17652.jpeg "20150405133914311.jpg")


#### 安装教程

html直接使用浏览器打开就可以

#### 使用说明

html直接使用浏览器打开就可以


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
